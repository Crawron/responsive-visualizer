export function getMin(array: Float32Array): number {
	let minValue = array[0]

	array.forEach((element: number) => {
		if (element < minValue) minValue = element
	})

	return minValue
}

export function getMax(array: Float32Array): number {
	let maxValue = array[0]

	array.forEach((element: number) => {
		if (element > maxValue) maxValue = element
	})

	return maxValue
}