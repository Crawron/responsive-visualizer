import { cap } from "./cap"

export function mapRange(minOrigin: number, maxOrigin: number, minTarget: number, maxTarget: number, value: number, constrain?: boolean): number {
	const targetRangeLength = maxTarget - minTarget
	const originRangeLength = maxOrigin - minOrigin
	const valueLength = value - minOrigin

	let result = valueLength * targetRangeLength / originRangeLength + minTarget

	if (constrain) {
		if (minTarget < maxTarget) result = cap(minTarget, maxTarget, result)
		else result = cap(maxTarget, minTarget, result)
	}

	return result
}