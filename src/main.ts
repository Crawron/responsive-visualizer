import { mapRange } from "./helpers/mapRange"
import { getMin, getMax } from "./helpers/minAndMax";


const audioCtx = new AudioContext()

// Create audio source
const audioElement = new Audio()
audioElement.src = 'audio.mp3'
audioElement.autoplay = true
audioElement.preload = 'auto'
const audioSourceNode = audioCtx.createMediaElementSource(audioElement)

//Create analyser node
const analyser = audioCtx.createAnalyser()
analyser.fftSize = 256 / 2
analyser.smoothingTimeConstant = .8
const bufferLength = analyser.frequencyBinCount
const dataArray = new Float32Array(bufferLength)

//Set up audio node network
audioSourceNode.connect(analyser)
analyser.connect(audioCtx.destination)


const canvas = <HTMLCanvasElement>document.getElementById("visualizer")
const context = <CanvasRenderingContext2D>canvas.getContext("2d")

const barStyle = "rgba(255,255,255,.5)"
const thickness = 2
const minSpacing = 5
const margin = 32

let internalWidth = canvas.width - margin * 2 - thickness
let barCount = internalWidth / (minSpacing + thickness)
let spacing = minSpacing + thickness + ((barCount % 1) * (minSpacing + thickness) / Math.floor(barCount))

function setup() {
	if (context) {
		analyser.getFloatFrequencyData(dataArray)
		calculateSpacing()
		render()
	}
}

function render() {
	context.clearRect(0, 0, canvas.width, canvas.height)
	analyser.getFloatFrequencyData(dataArray)

	for (var i = 0; i < barCount; i++) {
		const x = i * spacing + margin + thickness / 2
		const v = dataArray[i]

		renderBar(x, v)
	}


	requestAnimationFrame(render)
}

function resizeCanvas() {
	canvas.width = window.innerWidth
}

function calculateSpacing() {
	resizeCanvas()
	internalWidth = canvas.width - margin * 2 - thickness
	barCount = internalWidth / (minSpacing + thickness)
	spacing = minSpacing + thickness + ((barCount % 1) * (minSpacing + thickness) / Math.floor(barCount))
}

function renderBar(xPosition: number, height: number) {
	const yPosition = canvas.height - margin

	const maxHeight = (canvas.height - (margin * 2))
	const minValue = -140
	const maxValue = -20
	const finalHeight = mapRange(minValue, maxValue, yPosition - thickness, margin, height, true)

	context.beginPath()
	context.moveTo(xPosition, yPosition)
	context.lineTo(xPosition, finalHeight)

	context.lineWidth = thickness
	context.strokeStyle = barStyle
	context.stroke()
}


window.addEventListener("resize", calculateSpacing)
setup()

